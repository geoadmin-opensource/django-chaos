# coding: utf-8
try:
    from unittest.mock import MagicMock
except ImportError:
    from mock import MagicMock
from django.test import SimpleTestCase
from django.contrib.auth import get_user_model
from tests.utils import ChaosTestCase
from chaos.signals import task_status_changed
from model_mommy import mommy
from chaos.models import (
    Project,
    Task,
    TaskStatus,
)
from chaos.utils import (
    TaskHandler,
    TaskChangeDetector,
    TaskChange,
    get_responsibles,
    get_output_path,
)
User = get_user_model()


class GetOutputPathTestCase(SimpleTestCase):

    def test_raises_without_directory(self):
        with self.assertRaises(ValueError):
            get_output_path(None, 'lalala')

    def test_raises_without_filename(self):
        with self.assertRaises(ValueError):
            get_output_path('alalal', None)

    def test_raises_without_correct_filename(self):
        with self.assertRaises(ValueError):
            get_output_path('lalala', 'lalala')

    def test_generates_filename(self):
        output = get_output_path('/opt', 'foo.txt')
        self.assertIsNotNone(output)
        self.assertTrue(output.startswith('/opt/foo'))
        self.assertTrue(output.endswith('.txt'))


class GetResponsiblesTestCase(ChaosTestCase):

    def test_get_responsible_with_no_changes_returns_none(self):

        responsibles = get_responsibles(None)
        self.assertIsNone(responsibles)

    def test_get_responsible(self):
        user1 = mommy.make(User)
        user1 = mommy.make(User)
        user2 = mommy.make(User)
        proj1 = mommy.make(Project)
        t1 = Task(
            project=proj1,
            name='foo',
            responsible=user1
        )
        t1.save()
        t11 = Task(
            project=proj1,
            name='foo1',
            parent=t1,
            responsible=user2
        )
        t11.save()
        t2 = Task(
            project=proj1,
            name='bar'
        )
        t2.save()
        handler = TaskHandler()
        changes = handler.to_status(t1, self.closed, dry_run=True)
        users = get_responsibles(changes)
        self.assertEqual(len(users), 2)

    def test_get_single_responsible(self):
        user1 = mommy.make(User)
        proj1 = mommy.make(Project, responsible=user1)
        t1 = Task(
            project=proj1,
            name='foo',
            responsible=user1
        )
        t1.save()
        t11 = Task(
            project=proj1,
            name='foo1',
            parent=t1,
            responsible=user1
        )
        t11.save()
        t2 = Task(
            project=proj1,
            name='bar'
        )
        t2.save()
        handler = TaskHandler()
        changes = handler.to_status(t1, self.closed, dry_run=True)
        users = get_responsibles(changes)
        self.assertEqual(len(users), 1)

    def test_get_responsible_with_project(self):
        user1 = mommy.make(User)
        user2 = mommy.make(User)
        user3 = mommy.make(User)
        proj1 = mommy.make(Project, responsible=user3)
        t1 = Task(
            project=proj1,
            name='foo',
            responsible=user1
        )
        t1.save()
        t11 = Task(
            project=proj1,
            name='foo1',
            parent=t1,
            responsible=user2
        )
        t11.save()
        t2 = Task(
            project=proj1,
            name='bar'
        )
        t2.save()
        handler = TaskHandler()
        changes = handler.to_status(t1, self.closed, dry_run=True)
        users = get_responsibles(changes)
        self.assertEqual(len(users), 3)

    def test_get_responsible_without_project(self):
        user1 = mommy.make(User)
        user2 = mommy.make(User)
        user3 = mommy.make(User)
        proj1 = mommy.make(Project, responsible=user3)
        t1 = Task(
            project=proj1,
            name='foo',
            responsible=user1
        )
        t1.save()
        t11 = Task(
            project=proj1,
            name='foo1',
            parent=t1,
            responsible=user2
        )
        t11.save()
        t2 = Task(
            project=proj1,
            name='bar'
        )
        t2.save()
        handler = TaskHandler()
        changes = handler.to_status(t1, self.closed, dry_run=True)
        users = get_responsibles(changes, include_project=False)
        self.assertEqual(len(users), 2)


class TaskChangeDetectorTestCase(ChaosTestCase):

    def test_to_status_without_task(self):
        """
        Test to make sure that to_status cannot be called
        without a task
        """
        handler = TaskChangeDetector()

        with self.assertRaises(ValueError):
            handler.detect(None, self.closed)

    def test_to_status_without_status(self):
        """
        Test to make sure that to_status cannot be called
        without a target status
        """
        proj1 = mommy.make(Project)
        task1 = Task(
            project=proj1,
            name='foo'
        )
        handler = TaskChangeDetector()

        with self.assertRaises(ValueError):
            handler.detect(task1, None)

    def test_change_to_the_same_status_should_return_none(self):
        """
        Guarantees that calls to to_status will call
        the correct sub-method. In this case, we make
        sure that a open task that is changed to closed
        status, will call _close
        """
        proj1 = mommy.make(Project)
        task1 = Task(
            project=proj1,
            name='foo'
        )
        task1.save()
        handler = TaskChangeDetector()
        changes = handler.detect(task1, self.open)
        self.assertIsNone(changes)

    def test_single_task(self):

        """
        scenario: parent task and child task.
        user tries to close child task. we should get two changes,
        one for the child and the parent that does not have
        open child anymopre
        """
        proj1 = mommy.make(Project)
        task1 = Task.objects.create(
            project=proj1,
            name='foo'
        )
        task1.save()
        task2 = Task.objects.create(
            project=proj1,
            name='bar',
            parent=task1
        )
        handler = TaskChangeDetector()
        changes = handler.detect(task2, self.closed)
        self.assertIsNotNone(changes)
        self.assertEqual(len(changes.related_changes), 1)
        self.assertEqual(changes.initial_target.id, task2.id)

    def test_single_task_to_same_same_status_type(self):

        """
        scenario: parent task and child task.
        user tries to close child task. we should get one change.
        """
        proj1 = mommy.make(Project)
        task1 = Task(
            project=proj1,
            name='foo'
        )
        task1.save()
        task2 = Task(
            project=proj1,
            name='bar',
            parent=task1
        )
        execution_status = TaskStatus.objects.create(code='EXECUTION', is_closing_status=False)
        handler = TaskChangeDetector()
        changes = handler.detect(task2, execution_status)
        self.assertIsNotNone(changes)
        self.assertEqual(len(changes.related_changes), 0)

    def test_to_closing_status(self):
        """
        Guarantees that calls to to_status will call
        the correct sub-method. In this case, we make
        sure that a open task that is changed to closed
        status, will call _close
        """
        proj1 = mommy.make(Project)
        task1 = Task(
            project=proj1,
            name='foo'
        )
        task1.save()
        handler = TaskChangeDetector()
        mock_change = TaskChange(task1, 'closing', self.closed)
        handler._close = MagicMock(return_value=mock_change)
        changes = handler.detect(task1, self.closed)
        self.assertIsNotNone(changes)
        handler._close.assert_called_with(task1, self.closed, task1)

    def test_to_opening_status(self):
        """
        Guarantees that calls to detect will call
        the correct sub-method. In this case, we make
        sure that a closed task that is changed to an
        open status, will call _open
        """
        proj1 = mommy.make(Project)
        task1 = Task(
            project=proj1,
            name='foo',
            status=self.closed
        )
        task1.save()
        handler = TaskChangeDetector()
        mock_change = TaskChange(task1, 'opening', self.open)
        handler._open = MagicMock(return_value=mock_change)
        changes = handler.detect(task1, self.open)
        self.assertIsNotNone(changes)
        handler._open.assert_called_with(task1, self.open, task1)

    def test_open_opened_returns_none(self):
        """
        This test makes sure that it's not possible to change
        from an open state to another open state if allow_reopen=False
        """
        new_opening = mommy.make(TaskStatus, is_closing_status=False)
        proj1 = mommy.make(Project)
        t1 = Task(
            project=proj1,
            name='foo',
        )
        t1.save()
        handler = TaskChangeDetector(allow_all=False)
        changes = handler.detect(t1, new_opening)
        self.assertIsNone(changes)

    def test_open_opened_returns_changes_with_allow_reopen(self):
        """
        This test makes sure that it's not possible to change
        from an open state to another open state if allow_reopen=False
        """
        new_opening = mommy.make(TaskStatus, is_closing_status=False)
        proj1 = mommy.make(Project)
        t1 = Task(
            project=proj1,
            name='foo',
        )
        t1.save()
        handler = TaskChangeDetector()
        changes = handler.detect(t1, new_opening)
        self.assertIsNotNone(changes)

    def test_open_should_return_correct_changes1(self):
        """
        Scenario:
        two root nodes (A and B), without any children, both closed.
        open node A and only get changes for node A.
        no changes for node B
        """
        proj1 = mommy.make(Project)
        t1 = Task(
            project=proj1,
            name='foo',
            status=self.closed
        )
        t1.save()
        t2 = Task(
            project=proj1,
            name='bar',
            status=self.closed
        )
        t2.save()
        handler = TaskChangeDetector()
        changes = handler.detect(t1, self.open)
        self.assertEqual(changes.status, self.open)
        self.assertEqual(changes.task.pk, t1.pk)
        self.assertEqual(changes.initial_target, t1)
        self.assertEqual(len(changes.related_changes), 0)

    def test_open_should_return_correct_changes2(self):
        """
        Scenario:
        two root nodes, both closed.
        root A has one child, A1, closed as well.
        we reopen A1, which should reopen A
        """
        proj1 = mommy.make(Project)
        t1 = Task(
            project=proj1,
            name='foo',
            status=self.closed
        )
        t1.save()
        t11 = Task(
            project=proj1,
            parent=t1,
            name='foo1',
            status=self.closed
        )
        t11.save()
        t2 = Task(
            project=proj1,
            name='bar',
            status=self.closed
        )
        t2.save()
        handler = TaskChangeDetector()
        changes = handler.detect(t11, self.open)
        self.assertEqual(changes.status, self.open)
        self.assertEqual(changes.task.pk, t11.pk)
        self.assertEqual(changes.initial_target, t11)
        self.assertEqual(len(changes.related_changes), 1)
        self.assertEqual(changes.related_changes[0].task.pk, t1.pk)

    def test_open_should_return_correct_changes3(self):
        """
        Scenario:
        - A
          |- A1
             | - A11
        - B
        All nodes are closed. We try to reopen A11, which should reopen
        A1 and A
        """
        proj1 = mommy.make(Project)
        t1 = Task(
            project=proj1,
            name='foo',
            status=self.closed
        )
        t1.save()
        t11 = Task(
            project=proj1,
            parent=t1,
            name='foo1',
            status=self.closed
        )
        t11.save()
        t111 = Task(
            project=proj1,
            parent=t11,
            name='foo1',
            status=self.closed
        )
        t11.save()
        t2 = Task(
            project=proj1,
            name='bar',
            status=self.closed
        )
        t2.save()
        handler = TaskChangeDetector()
        changes = handler.detect(t111, self.open)
        self.assertEqual(changes.status, self.open)
        self.assertEqual(changes.task.pk, t111.pk)
        self.assertEqual(changes.initial_target, t111)
        self.assertEqual(len(changes.related_changes), 1)
        self.assertEqual(changes.related_changes[0].task.pk, t11.pk)
        self.assertEqual(changes.related_changes[0].status, self.open)
        self.assertEqual(changes.related_changes[0].related_changes[0].status, self.open)
        self.assertEqual(changes.related_changes[0].related_changes[0].initial_target, t111)

    def test_close_closed_returns_none(self):
        new_closing = mommy.make(TaskStatus, is_closing_status=True)
        proj1 = mommy.make(Project)
        t1 = Task(
            project=proj1,
            name='foo',
            status=self.closed
        )
        t1.save()
        handler = TaskChangeDetector(allow_all=False)
        changes = handler.detect(t1, new_closing)
        self.assertIsNone(changes)

    def test_close_closed_returns_changes_with_allow_reclose(self):
        new_closing = mommy.make(TaskStatus, is_closing_status=True)
        proj1 = mommy.make(Project)
        t1 = Task(
            project=proj1,
            name='foo',
            status=self.closed
        )
        t1.save()
        handler = TaskChangeDetector()
        changes = handler.detect(t1, new_closing)
        self.assertIsNotNone(changes)

    def test_close_should_return_correct_changes1(self):
        """
        Scenario:
        two root nodes (A and B), without any children.
        close node A and only get changes for node A.
        no changes for node B
        """
        proj1 = mommy.make(Project)
        t1 = Task(
            project=proj1,
            name='foo',
        )
        t1.save()
        t2 = Task(
            project=proj1,
            name='bar'
        )
        t2.save()
        handler = TaskChangeDetector()
        changes = handler.detect(t1, self.closed)
        self.assertEqual(changes.status, self.closed)
        self.assertEqual(changes.task.pk, t1.pk)
        self.assertEqual(changes.initial_target, t1)
        self.assertEqual(len(changes.related_changes), 0)

    def test_close_should_return_correct_changes2(self):
        """
        Scenario:
        two root nodes, A and B. A has one children.
        close root node A also closes child A1.
        no changes to B
        """
        proj1 = mommy.make(Project)
        t1 = Task(
            project=proj1,
            name='foo',
        )
        t1.save()
        t11 = Task(
            project=proj1,
            name='foo1',
            parent=t1
        )
        t11.save()
        t2 = Task(
            project=proj1,
            name='bar'
        )
        t2.save()
        handler = TaskChangeDetector()
        changes = handler.detect(t1, self.closed)
        self.assertEqual(changes.status, self.closed)
        self.assertEqual(changes.task.pk, t1.pk)
        self.assertEqual(changes.initial_target, t1)
        self.assertEqual(len(changes.related_changes), 1)
        related_changes = changes.related_changes[0]
        self.assertEqual(related_changes.task.pk, t11.pk)
        self.assertEqual(related_changes.initial_target, t1)
        self.assertEqual(len(related_changes.related_changes), 0)

    def test_close_should_return_correct_changes3(self):
        """
        Scenario:
        two root nodes, A and B. A has one children.
        close A1 that will check for siblings and since
        there are none, it will close A, which will close A1.
        no changes to B
        """
        proj1 = mommy.make(Project)
        t1 = Task(
            project=proj1,
            name='foo',
        )
        t1.save()
        t11 = Task(
            project=proj1,
            name='foo1',
            parent=t1
        )
        t11.save()
        t2 = Task(
            project=proj1,
            name='bar'
        )
        t2.save()
        handler = TaskChangeDetector()
        changes = handler.detect(t11, self.closed)
        self.assertEqual(changes.status, self.closed)
        self.assertEqual(changes.task.pk, t1.pk)
        self.assertEqual(changes.initial_target, t11)
        self.assertEqual(len(changes.related_changes), 1)
        related_changes = changes.related_changes[0]
        self.assertEqual(related_changes.task.pk, t11.pk)
        self.assertEqual(related_changes.initial_target, t11)
        self.assertEqual(len(related_changes.related_changes), 0)


class TaskHandlerTestCase(ChaosTestCase):

    def test_init(self):

        handler = TaskHandler()
        self.assertIsNotNone(handler)
        self.assertTrue(isinstance(handler.detector, TaskChangeDetector))

    def test_init_different_detector(self):

        handler = TaskHandler(detector_class=str)
        self.assertIsNotNone(handler)
        self.assertTrue(isinstance(handler.detector, str))

    def test_flatten_empty_change(self):

        handler = TaskHandler()
        changes = handler._flatten(None)
        self.assertTrue(isinstance(changes, list))
        self.assertEqual(len(changes), 0)

    def test_flatten1(self):
        handler = TaskHandler()
        c1 = TaskChange('foo', 'bar', 'foo')
        c11 = TaskChange('foo1', 'bar', 'foo')
        c12 = TaskChange('foo2', 'bar', 'foo')
        c1.related_changes.extend([c11, c12])
        changes = handler._flatten(c1)
        self.assertEqual(len(changes), 3)
        tasks = [c.task for c in changes]
        self.assertEquals(tasks, ['foo', 'foo1', 'foo2'])

    def test_flatten2(self):
        c1 = TaskChange('foo', 'bar', 'foo')
        c11 = TaskChange('foo1', 'bar', 'foo')
        c12 = TaskChange('foo2', 'bar', 'foo')
        c2 = TaskChange('bar', 'bar', 'foo')
        c21 = TaskChange('bar1', 'bar', 'foo')
        c22 = TaskChange('bar2', 'bar', 'foo')
        c2.related_changes.extend([c21, c22])
        c1.related_changes.extend([c11, c12, c2])
        changes = TaskHandler.flatten(c1)
        self.assertEqual(len(changes), 6)
        tasks = [c.task for c in changes]
        self.assertEquals(tasks, ['foo', 'foo1', 'foo2', 'bar', 'bar1', 'bar2'])

    def test_to_status_dry_run_does_not_apply_changes(self):
        """
        Test to make sure that dry_run param does not apply
        changes to the tasks.
        """
        proj1 = mommy.make(Project)
        t1 = Task(
            project=proj1,
            name='foo',
        )
        t1.save()
        t11 = Task(
            project=proj1,
            name='foo1',
            parent=t1
        )
        t11.save()
        t2 = Task(
            project=proj1,
            name='bar'
        )
        t2.save()
        handler = TaskHandler()
        changes = handler.to_status(t1, self.closed, dry_run=True)
        self.assertIsNotNone(changes)
        flat_changes = TaskHandler.flatten(changes)
        self.assertEqual(len(flat_changes), 2)
        self.assertEqual(t1.status, self.open)

    def test_to_status_applies_changes(self):
        """
        Test to make sure that dry_run param does not apply
        changes to the tasks.
        """
        proj1 = mommy.make(Project)
        t1 = Task(
            project=proj1,
            name='foo',
        )
        t1.save()
        t11 = Task(
            project=proj1,
            name='foo1',
            parent=t1
        )
        t11.save()
        t2 = Task(
            project=proj1,
            name='bar'
        )
        t2.save()
        handler = TaskHandler()
        changes = handler.to_status(t1, self.closed)
        self.assertIsNotNone(changes)
        flat_changes = TaskHandler.flatten(changes)
        self.assertEqual(len(flat_changes), 2)

        task1 = Task.objects.get(name='foo')
        self.assertEqual(task1.status, self.closed)
        children = task1.get_children()
        self.assertEqual(children.count(), 1)
        self.assertEqual(children[0].status, self.closed)

    def test_to_status_triggers_signal(self):
        """
        Test to make sure that dry_run param does not apply
        changes to the tasks.
        """
        receiver = MagicMock()
        task_status_changed.connect(receiver)
        proj1 = mommy.make(Project)
        t1 = Task(
            project=proj1,
            name='foo',
        )
        t1.save()
        t11 = Task(
            project=proj1,
            name='foo1',
            parent=t1
        )
        t11.save()
        t2 = Task(
            project=proj1,
            name='bar'
        )
        t2.save()
        handler = TaskHandler()
        changes = handler.to_status(t1, self.closed)
        self.assertIsNotNone(changes)
        flat_changes = TaskHandler.flatten(changes)
        self.assertEqual(len(flat_changes), 2)

        task1 = Task.objects.get(name='foo')
        self.assertEqual(task1.status, self.closed)
        children = task1.get_children()
        self.assertEqual(children.count(), 1)
        self.assertEqual(children[0].status, self.closed)
        receiver.assert_called_once()
