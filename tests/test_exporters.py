# coding: utf-8
from collections import OrderedDict
from datetime import timedelta
from django.test import SimpleTestCase
from django.contrib.auth import get_user_model
from django.utils.timezone import now
from model_mommy import mommy
from tests.utils import ChaosTestCase
from chaos.models import (
    Project,
    Task,
)
from chaos.exporters import (
    TaskExport,
    XLSXTaskExport,
    ICALTaskExport,
)
User = get_user_model()


class TaskExportTestCase(SimpleTestCase):

    def test_init(self):

        ex = TaskExport()
        self.assertIsNotNone(ex)
        self.assertTrue(ex.publish_url)
        self.assertIsNone(ex.request)
        self.assertEqual(ex.default_filename, 'task-export.txt')
        self.assertEqual(ex.content_type, 'text/text')

    def test_export_fails(self):
        with self.assertRaises(NotImplementedError):
            ex = TaskExport()
            ex.export('foo')

    def test_respond_fails(self):
        with self.assertRaises(NotImplementedError):
            ex = TaskExport()
            ex.respond('foo')


class XLSXTaskExportTestCase(ChaosTestCase):

    def setUp(self):
        super(XLSXTaskExportTestCase, self).setUp()
        self.exporter = XLSXTaskExport()

    def test_init(self):

        self.assertIsNotNone(self.exporter)
        self.assertTrue(self.exporter.publish_url)
        self.assertIsNone(self.exporter.request)
        self.assertEqual(self.exporter.content_type, 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')  # noqa
        self.assertEqual(self.exporter.default_filename, 'tasks.xlsx')

    def test_serialize_task(self):

        project = mommy.make(Project)
        sd = now()
        ed = now() + timedelta(days=1)
        user = mommy.make(User)
        task = Task.objects.create(
            project=project,
            name='foo',
            description='text',
            start_date=sd,
            end_date=ed,
            responsible=user
        )
        event = self.exporter._serialize_task(task)
        self.assertIsNotNone(event)
        self.assertTrue(isinstance(event, OrderedDict))

    def test_export(self):

        project = mommy.make(Project)
        sd = now()
        ed = now() + timedelta(days=1)
        user = mommy.make(User)
        Task.objects.create(
            project=project,
            name='foo',
            description='text',
            start_date=sd,
            end_date=ed,
            responsible=user
        )
        output_file = self.exporter.export(Task.objects.all())
        self.assertIsNotNone(output_file)


class ICALTaskExportTestCase(ChaosTestCase):

    def setUp(self):
        super(ICALTaskExportTestCase, self).setUp()
        self.ical = ICALTaskExport()

    def test_init(self):

        exporter = ICALTaskExport()
        self.assertIsNotNone(exporter)
        self.assertTrue(exporter.publish_url)
        self.assertIsNone(exporter.request)
        self.assertEqual(exporter.content_type, 'text/calendar')
        self.assertEqual(exporter.default_filename, 'tasks.ics')

    def test_init_with_variables(self):
        exporter = ICALTaskExport(
            publish_url=False,
            default_filename='foo.ics',
            request='foo'
        )
        self.assertIsNotNone(exporter)
        self.assertIsNotNone(exporter.request)
        self.assertEqual(exporter.request, 'foo')
        self.assertEqual(exporter.default_filename, 'foo.ics')

    def test_create_attendee_without_role(self):

        user = mommy.make(User)
        attendee = self.ical._create_attendee(user)
        self.assertIsNotNone(attendee)
        self.assertEqual(attendee.params['ROLE'], 'REQ-PARTICIPANT')

    def test_create_attendee_with_role(self):

        user = mommy.make(User)
        attendee = self.ical._create_attendee(user, role='CHAIR')
        self.assertIsNotNone(attendee)
        self.assertEqual(attendee.params['ROLE'], 'CHAIR')

    def test_get_task_url_without_publish(self):
        self.ical = ICALTaskExport(publish_url=False)
        project = mommy.make(Project)
        sd = now()
        ed = now() + timedelta(days=1)
        user = mommy.make(User)
        task = Task.objects.create(
            project=project,
            name='foo',
            description='text',
            start_date=sd,
            end_date=ed,
            responsible=user
        )
        url = self.ical._get_task_url(task)
        self.assertIsNotNone(url)
        self.assertEqual(url, '')

    def test_get_event_url(self):
        project = mommy.make(Project)
        sd = now()
        ed = now() + timedelta(days=1)
        user = mommy.make(User)
        task = Task.objects.create(
            project=project,
            name='foo',
            description='text',
            start_date=sd,
            end_date=ed,
            responsible=user
        )
        url = self.ical._get_task_url(task)
        self.assertIsNotNone(url)
        self.assertEqual(url, '/api/tasks/{0}'.format(task.pk))

    def test_create_event(self):

        project = mommy.make(Project)
        sd = now()
        ed = now() + timedelta(days=1)
        user = mommy.make(User)
        task = Task.objects.create(
            project=project,
            name='foo',
            description='text',
            start_date=sd,
            end_date=ed,
            responsible=user
        )
        event = self.ical._create_event(task)
        self.assertIsNotNone(event)

    def test_create_calendar(self):

        project = mommy.make(Project)
        sd = now()
        ed = now() + timedelta(days=1)
        user = mommy.make(User)
        task = Task.objects.create(
            project=project,
            name='foo',
            description='text',
            start_date=sd,
            end_date=ed,
            responsible=user
        )
        cal = self.ical._create_calendar(project=project, queryset=[task])
        self.assertIsNotNone(cal)

    def test_respond(self):
        project = mommy.make(Project)
        sd = now()
        ed = now() + timedelta(days=1)
        user = mommy.make(User)
        task = Task.objects.create(
            project=project,
            name='foo',
            description='text',
            start_date=sd,
            end_date=ed,
            responsible=user
        )
        cal = self.ical._create_calendar(project=project, queryset=[task])
        response = self.ical.respond(Task.objects.all())
        self.assertIsNotNone(response)
        self.assertEqual(response.content, cal.to_ical())
