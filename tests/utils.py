# coding: utf-8
import os
import shutil
from django.conf import settings
from django.contrib.auth import get_user_model
from rest_framework.test import APITransactionTestCase
from django.test import TransactionTestCase
from chaos.data_migrations import create_task_status
from chaos.models import TaskStatus
from model_mommy import mommy
User = get_user_model()


def create_media():
    if not os.path.isdir(settings.MEDIA_ROOT):
        os.makedirs(os.path.join(settings.MEDIA_ROOT, 'export', 'chaos'))


def remove_media():
    if os.path.isdir(settings.MEDIA_ROOT):
        shutil.rmtree(settings.MEDIA_ROOT)


def create_status(test_instance):
    create_task_status()
    test_instance.open = TaskStatus.objects.get(code='OPEN')
    test_instance.closed = TaskStatus.objects.get(code='CLOSED')


class ChaosAPITestCase(APITransactionTestCase):
    def setUp(self):
        create_status(self)
        user = mommy.make(User, username='test')
        user.set_password('test')
        user.save()
        self.client.login(username='test', password='test')

    @classmethod
    def setUpClass(cls):
        super(ChaosAPITestCase, cls).setUpClass()
        create_media()

    @classmethod
    def tearDownClass(cls):
        super(ChaosAPITestCase, cls).tearDownClass()
        remove_media()


class ChaosTestCase(TransactionTestCase):

    def setUp(self):
        create_status(self)

    @classmethod
    def setUpClass(cls):
        super(ChaosTestCase, cls).setUpClass()
        create_media()

    @classmethod
    def tearDownClass(cls):
        super(ChaosTestCase, cls).tearDownClass()
        remove_media()
