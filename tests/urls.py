# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import

from django.conf.urls import url, include

from chaos.urls import urlpatterns as chaos_urls

urlpatterns = [
    url(r'', include(chaos_urls)),
]
