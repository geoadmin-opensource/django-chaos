# coding: utf-8
from datetime import datetime
import pytz
from collections import OrderedDict
from django.contrib.auth import get_user_model
from model_mommy import mommy
from tests.utils import ChaosTestCase
from chaos.serializers import (
    TaskChangeSerializer,
    AllocationSerializer,
)
from chaos.allocation import (
    Allocation,
    AllocationHandler,
)
from chaos.utils import TaskChange
from chaos.models import (
    Project,
    Task,
)
User = get_user_model()


class TaskChangeSerializerTestCase(ChaosTestCase):

    def test_init(self):

        tc_serializer = TaskChangeSerializer()
        self.assertIsNotNone(tc_serializer)

    def test_init_with_task_change(self):
        project = mommy.make(Project)
        task = Task(
            project=project,
            name='foo',
        )
        task.save()
        task_change = TaskChange(
            task=task,
            initial_target=task,
            action='closing',
            status=self.closed
        )
        tc_serializer = TaskChangeSerializer(task_change)
        data = tc_serializer.data
        self.assertIsNotNone(data)
        self.assertTrue(isinstance(data, OrderedDict))


class AllocationSerializerTestCase(ChaosTestCase):

    def test_init(self):

        ac_serializer = AllocationSerializer()
        self.assertIsNotNone(ac_serializer)

    def test_init_with_allocation(self):
        user = mommy.make(User)
        project = mommy.make(Project)
        task = Task(
            project=project,
            name='foo',
        )
        task.save()
        alloc = Allocation(
            user=user,
            date_point=datetime(2000, 1, 1, 0, 0, 0, tzinfo=pytz.utc),
            tasks=[task]
        )
        ac_serializer = AllocationSerializer(alloc)
        data = ac_serializer.data
        self.assertIsNotNone(data)
        self.assertTrue(isinstance(data, OrderedDict))
