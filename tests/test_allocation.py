# coding: utf-8
from datetime import datetime
import pytz
from django.contrib.auth import get_user_model
from model_mommy import mommy
from tests.utils import ChaosTestCase
from chaos.models import (
    Project,
    Task,
)
from chaos.allocation import AllocationHandler
User = get_user_model()


class AllocationHandlerTestCase(ChaosTestCase):

    def test_init(self):
        handler = AllocationHandler()
        self.assertIsNotNone(handler)

    def test_get_allocations_for_user_without_tasks(self):
        user = mommy.make(User)
        handler = AllocationHandler()
        allocs = handler.get_user_allocations(user)
        self.assertIsNotNone(allocs)
        self.assertEqual(len(allocs), 0)

    def test_get_allocations_for_user_with_tasks(self):

        user = mommy.make(User)
        project = mommy.make(Project)
        sd = datetime(2000, 1, 1, 0, 0, 0, tzinfo=pytz.utc)
        ed = datetime(2000, 1, 1, 10, 0, 0, tzinfo=pytz.utc)
        task = Task(
            name='foo',
            project=project,
            responsible=user,
            start_date=sd,
            end_date=ed
        )
        task.save()
        handler = AllocationHandler()
        allocs = handler.get_user_allocations(user)
        self.assertIsNotNone(allocs)
        self.assertEqual(len(allocs), 1)

    def test_get_allocations_for_user_with_tasks_different_dates(self):

        user = mommy.make(User)
        project = mommy.make(Project)
        sd1 = datetime(2000, 1, 1, 0, 0, 0, tzinfo=pytz.utc)
        ed1 = datetime(2000, 1, 1, 10, 0, 0, tzinfo=pytz.utc)
        sd2 = datetime(2001, 1, 1, 0, 0, 0, tzinfo=pytz.utc)
        ed2 = datetime(2001, 1, 1, 10, 0, 0, tzinfo=pytz.utc)
        task1 = Task(
            name='foo1',
            project=project,
            responsible=user,
            start_date=sd1,
            end_date=ed1
        )
        task1.save()
        task2 = Task(
            name='foo2',
            project=project,
            responsible=user,
            start_date=sd2,
            end_date=ed2
        )
        task2.save()
        handler = AllocationHandler()
        allocs = handler.get_user_allocations(user)
        self.assertIsNotNone(allocs)
        self.assertEqual(len(allocs), 2)
        actual = [a.tasks[0].pk for a in allocs]
        # this test was changed because there is a slight difference how py2 and
        # py3 orders dictionaries. and we are NOT enforcing allocation order. we
        # should, but we are not.
        self.assertIn(task1.pk, actual)
        self.assertIn(task2.pk, actual)

    def test_weight_for_allocations_should_be_correct(self):

        user = mommy.make(User)
        project = mommy.make(Project)
        sd1 = datetime(2000, 1, 1, 0, 0, 0, tzinfo=pytz.utc)
        ed1 = datetime(2000, 1, 1, 10, 0, 0, tzinfo=pytz.utc)
        sd2 = datetime(2001, 1, 1, 0, 0, 0, tzinfo=pytz.utc)
        ed2 = datetime(2001, 1, 1, 10, 0, 0, tzinfo=pytz.utc)
        task1 = Task(
            name='foo',
            project=project,
            responsible=user,
            start_date=sd1,
            end_date=ed1
        )
        task1.save()
        task2 = Task(
            name='foo',
            project=project,
            responsible=user,
            start_date=sd2,
            end_date=ed2
        )
        task2.save()
        handler = AllocationHandler()
        allocs = handler.get_user_allocations(user)
        self.assertIsNotNone(allocs)
        self.assertEqual(len(allocs), 2)
        self.assertEqual(allocs[0].weight, 1)
        self.assertEqual(allocs[1].weight, 1)

    def test_weight_should_be_summed_for_same_day_tasks(self):

        user = mommy.make(User)
        project = mommy.make(Project)
        sd1 = datetime(2000, 1, 1, 0, 0, 0, tzinfo=pytz.utc)
        ed1 = datetime(2000, 1, 1, 10, 0, 0, tzinfo=pytz.utc)
        sd2 = datetime(2000, 1, 1, 0, 0, 0, tzinfo=pytz.utc)
        ed2 = datetime(2000, 1, 1, 10, 0, 0, tzinfo=pytz.utc)
        task1 = Task(
            name='foo',
            project=project,
            responsible=user,
            start_date=sd1,
            end_date=ed1
        )
        task1.save()
        task2 = Task(
            name='foo',
            project=project,
            responsible=user,
            start_date=sd2,
            end_date=ed2,
            weight=10
        )
        task2.save()
        handler = AllocationHandler()
        allocs = handler.get_user_allocations(user)
        self.assertIsNotNone(allocs)
        self.assertEqual(len(allocs), 1)
        self.assertEqual(allocs[0].weight, 11)

    def test_get_allocations_for_users_with_tasks(self):

        user1 = mommy.make(User)
        user2 = mommy.make(User)
        project1 = mommy.make(Project)
        project2 = mommy.make(Project)
        sd = datetime(2000, 1, 1, 0, 0, 0, tzinfo=pytz.utc)
        ed = datetime(2000, 1, 1, 10, 0, 0, tzinfo=pytz.utc)
        task1 = Task(
            name='foo',
            project=project1,
            responsible=user1,
            start_date=sd,
            end_date=ed
        )
        task2 = Task(
            name='foo',
            project=project2,
            responsible=user2,
            start_date=sd,
            end_date=ed
        )
        task1.save()
        task2.save()
        handler = AllocationHandler()
        allocs = handler.get_user_allocations(user1)
        self.assertIsNotNone(allocs)
        self.assertEqual(len(allocs), 1)
        allocs = handler.get_user_allocations(user2)
        self.assertIsNotNone(allocs)
        self.assertEqual(len(allocs), 1)
