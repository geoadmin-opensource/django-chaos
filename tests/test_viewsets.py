# coding: utf-8
from datetime import timedelta
from django.utils.timezone import now
from django.contrib.auth import get_user_model
from rest_framework.reverse import reverse
from tests.utils import ChaosAPITestCase
from model_mommy import mommy
from chaos.models import (
    Project,
    Task,
)
User = get_user_model()


class AllocationViewSetTestCase(ChaosAPITestCase):

    def test_list(self):
        user = mommy.make(User)
        proj = mommy.make(Project, responsible=user)
        start = now()
        end = start + timedelta(days=2)
        task1 = Task.objects.create(
            name='foo',
            project=proj,
            responsible=user,
            start_date=now(),
            end_date=end
        )

        url = reverse('allocations-list')
        response = self.client.get(url)
        self.assertIsNotNone(response)
        self.assertEqual(response.status_code, 200)

        data = response.data
        self.assertEqual(len(data), 2)
        date1 = data[0]
        date2 = data[1]
        self.assertEqual(date1['tasks'][0]['id'], task1.pk)
        self.assertEqual(date2['tasks'][0]['id'], task1.pk)

    def test_retrieve(self):
        user = mommy.make(User)
        proj = mommy.make(Project, responsible=user)
        start = now()
        end = start + timedelta(days=2)
        task1 = Task.objects.create(
            name='foo',
            project=proj,
            responsible=user,
            start_date=now(),
            end_date=end
        )

        url = reverse('allocations-detail', kwargs={'pk': user.pk})
        response = self.client.get(url)
        self.assertIsNotNone(response)
        self.assertEqual(response.status_code, 200)

        data = response.data
        self.assertEqual(len(data), 2)
        date1 = data[0]
        date2 = data[1]
        self.assertEqual(date1['tasks'][0]['id'], task1.pk)
        self.assertEqual(date2['tasks'][0]['id'], task1.pk)

    def test_retrieve_user_without_allocation(self):
        user1 = mommy.make(User)
        user2 = mommy.make(User)
        proj = mommy.make(Project, responsible=user1)
        start = now()
        end = start + timedelta(days=2)
        Task.objects.create(
            name='foo',
            project=proj,
            responsible=user1,
            start_date=now(),
            end_date=end
        )

        url = reverse('allocations-detail', kwargs={'pk': user2.pk})
        response = self.client.get(url)
        self.assertIsNotNone(response)
        self.assertEqual(response.status_code, 200)

        self.assertEqual(len(response.data), 0)

    def test_retrieve_user_without_user(self):
        user1 = mommy.make(User)
        proj = mommy.make(Project, responsible=user1)
        start = now()
        end = start + timedelta(days=2)
        Task.objects.create(
            name='foo',
            project=proj,
            responsible=user1,
            start_date=now(),
            end_date=end
        )

        url = reverse('allocations-detail', kwargs={'pk': 9999})
        response = self.client.get(url)
        self.assertIsNotNone(response)
        self.assertEqual(response.status_code, 404)


class TaskViewSetTestCase(ChaosAPITestCase):

    def test_change_dry_run(self):
        proj = mommy.make(Project)
        task = Task.objects.create(
            name='foo',
            project=proj
        )

        url = reverse('task-change-status', kwargs={'pk': task.pk})
        response = self.client.post(url, {
            'status': self.closed.pk,
            'dry_run': True
        })
        self.assertIsNotNone(response)
        self.assertEqual(response.status_code, 200)
        # makes sure that dry run is working!
        self.assertEqual(Task.objects.filter(status=self.open).count(), 1)  # noqa

    def test_change_wet_run(self):
        proj = mommy.make(Project)
        task = Task.objects.create(
            name='foo',
            project=proj
        )

        url = reverse('task-change-status', kwargs={'pk': task.pk})
        response = self.client.post(url, {
            'status': self.closed.pk,
            'dry_run': False
        })
        self.assertIsNotNone(response)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(Task.objects.filter(status=self.open).count(), 0)  # noqa
        self.assertEqual(Task.objects.filter(status=self.closed).count(), 1)  # noqa

    def test_score(self):
        proj = mommy.make(Project)
        task = Task.objects.create(
            name='foo',
            project=proj
        )

        url = reverse('task-score', kwargs={'pk': task.pk})
        response = self.client.get(url)
        self.assertIsNotNone(response)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data, {'task': task.pk, 'score': 0})

    def test_attach(self):
        proj = mommy.make(Project)
        task1 = Task.objects.create(
            name='foo',
            project=proj
        )
        task2 = Task.objects.create(
            name='foo',
            project=proj
        )

        url = reverse('task-attach', kwargs={'pk': task1.pk})
        response = self.client.post(url, {
            'model_name': 'chaos.Task',
            'model_id': task2.pk
        })
        self.assertIsNotNone(response)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(task1.attachments.all().count(), 1)

    def test_detach(self):

        proj = mommy.make(Project)
        task1 = Task.objects.create(
            name='foo',
            project=proj
        )
        task2 = Task.objects.create(
            name='foo',
            project=proj
        )

        attachUrl = reverse('task-attach', kwargs={'pk': task1.pk})
        detachUrl = reverse('task-detach', kwargs={'pk': task1.pk})
        response = self.client.post(attachUrl, {
            'model_name': 'chaos.Task',
            'model_id': task2.pk
        })
        self.assertIsNotNone(response)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(task1.attachments.all().count(), 1)

        response = self.client.post(detachUrl, {
            'model_name': 'chaos.Task',
            'model_id': task2.pk
        })

        self.assertIsNotNone(response)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(task1.attachments.all().count(), 0)
        self.assertEqual(response.data, {'status': 'Succeeded', 'message': 'Detach succeeded'})

    def test_clone(self):

        proj1 = mommy.make(Project)
        proj2 = mommy.make(Project)
        task1 = Task.objects.create(
            name='foo',
            project=proj1
        )
        task2 = Task.objects.create(
            name='foo',
            project=proj1
        )

        cloneUrl = reverse('task-clone', kwargs={'pk': task1.pk})
        response = self.client.post(cloneUrl, {
            'new_project': proj2.pk
        })
        self.assertIsNotNone(response)
        self.assertEqual(response.status_code, 201)
        self.assertEqual(proj2.tasks.all().count(), 1)


class ProjectViewSetTestCase(ChaosAPITestCase):

    def test_clone(self):
        proj = mommy.make(Project, name='foo')
        Task.objects.create(
            name='foo',
            project=proj
        )

        url = reverse('project-clone', kwargs={'pk': proj.pk})
        response = self.client.post(url, {
            'new_name': 'bar',
        })
        self.assertIsNotNone(response)
        self.assertEqual(response.status_code, 201)
        self.assertEqual(Project.objects.all().count(), 2)
        self.assertEqual(Task.objects.all().count(), 2)

    def test_score(self):
        proj = mommy.make(Project)
        Task.objects.create(
            name='foo',
            project=proj
        )
        url = reverse('project-score', kwargs={'pk': proj.pk})
        response = self.client.get(url)
        self.assertIsNotNone(response)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data, {'project': proj.pk, 'score': 0})
