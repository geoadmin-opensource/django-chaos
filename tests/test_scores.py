# coding: utf-8
from django.test import TransactionTestCase
from model_mommy import mommy
from chaos.data_migrations import create_task_status
from chaos.models import Task, TaskStatus, Project
from chaos.scores import (
    calculate_node_score,
    calculate_project_score,
)


class CalculateNodeScoreTestCase(TransactionTestCase):

    """
    Tests for the calculate_node_score function.
    this uses model_mommy AND creates some models
    by hand because of the model_mommy behavior, which
    is constructing extra stuff we dont need.
    """

    def setUp(self):
        create_task_status()
        self.open = TaskStatus.objects.get(code='OPEN')
        self.closed = TaskStatus.objects.get(code='CLOSED')

    def test_calculate_score_empty_node(self):

        score = calculate_node_score(None)
        self.assertIsNotNone(score)

    def test_calculate_score_single_node(self):

        parent = mommy.make(Task)
        score = calculate_node_score(parent)
        self.assertIsNotNone(score)
        self.assertEqual(score, 0)

    def test_calculate_without_weights_should_be_correct(self):
        project = mommy.make(Project)
        parent = Task(project=project, name='foo')
        parent.save()
        c1 = Task(project=project, name='foo1', parent=parent)
        c1.save()
        c2 = Task(project=project, name='foo2', parent=parent, status=self.closed)
        c2.save()
        score = calculate_node_score(parent)
        self.assertEqual(score, 0.5)

    def test_calculate_with_weights_should_be_correct(self):
        project = mommy.make(Project)
        parent = Task(project=project, name='foo')
        parent.save()
        c1 = Task(project=project, name='foo1', parent=parent, weight=9)
        c1.save()
        c2 = Task(project=project, name='foo2', parent=parent, status=self.closed)
        c2.save()
        score = calculate_node_score(parent)
        self.assertAlmostEqual(score, 0.1, 2)

    def test_calculate_with_grandchildren_should_be_correct(self):
        project = mommy.make(Project)
        parent = Task(project=project, name='foo')
        parent.save()
        c1 = Task(project=project, name='foo1', parent=parent)
        c1.save()
        c11 = Task(project=project, name='foo11', parent=c1)
        c11.save()
        c12 = Task(project=project, name='foo12', parent=c1)
        c12.save()
        c2 = Task(project=project, name='foo2', parent=parent, status=self.closed)
        c2.save()
        score = calculate_node_score(parent)
        self.assertAlmostEqual(score, 0.33, 2)


class CalculateProjectScoreTestCase(TransactionTestCase):

    def setUp(self):
        create_task_status()
        self.open = TaskStatus.objects.get(code='OPEN')
        self.closed = TaskStatus.objects.get(code='CLOSED')

    def test_calculate_without_project_raises_error(self):

        with self.assertRaises(ValueError):
            calculate_project_score(None)

    def test_calculate_with_project_should_return_score(self):

        project = mommy.make(Project)
        score = calculate_project_score(project)
        self.assertIsNotNone(score)
        self.assertEqual(score, 0)

    def test_calculate_with_two_root(self):

        project = mommy.make(Project)
        t1 = Task(project=project, name='foo1')
        t1.save()
        t2 = Task(project=project, name='foo2')
        t2.save()
        score = calculate_project_score(project)
        self.assertIsNotNone(score)
        self.assertEqual(score, 0)

    def test_calculate_with_two_root_one_closed(self):
        project = mommy.make(Project)
        t1 = Task(project=project, name='foo1', status=self.closed)
        t1.save()
        t2 = Task(project=project, name='foo2')
        t2.save()
        score = calculate_project_score(project)
        self.assertIsNotNone(score)
        self.assertEqual(score, 0.5)

    def test_calculate_with_two_root_closed(self):
        project = mommy.make(Project)
        t1 = Task(project=project, name='foo1', status=self.closed)
        t1.save()
        t2 = Task(project=project, name='foo2', status=self.closed)
        t2.save()
        score = calculate_project_score(project)
        self.assertIsNotNone(score)
        self.assertEqual(score, 1)
