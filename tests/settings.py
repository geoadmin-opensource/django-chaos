# -*- coding: utf-8
from __future__ import unicode_literals, absolute_import
import os
import django

DEBUG = True
USE_TZ = True
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = "xpp(m=k#yq#c(gdstvx73a(i-6n$0f_-+4_@$)+&!px+p33rf9"

DATABASES = {
    "default": {
        'ENGINE': 'django.contrib.gis.db.backends.spatialite',
        'NAME': 'tests.db'
    }
}

SPATIALITE_LIBRARY_PATH = 'mod_spatialite'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'APP_DIRS': True,
        'DIRS': [
            os.path.join(BASE_DIR, "templates")
        ],

        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.media',
                'django.contrib.gis',
            ],
        },
    },
]

STATIC_URL = '/static/'
MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
ROOT_URLCONF = "tests.urls"
LOCALE_PATHS = (
    os.path.join(BASE_DIR, 'chaos', 'locale'),
)
INSTALLED_APPS = [
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.staticfiles",
    "django.contrib.sessions",
    "django.contrib.sites",
    "mptt",
    "common",
    "file_context",
    "chaos",
    "rest_framework",
    "rest_framework.authtoken",
    "django_filters",
    "rest_framework_filters",
    "taggit",
    "taggit_serializer",
]

SITE_ID = 1

if django.VERSION >= (1, 10):
    MIDDLEWARE = ()
else:
    MIDDLEWARE_CLASSES = ()

REST_FRAMEWORK = {
    'DEFAULT_FILTER_BACKENDS': ('rest_framework_filters.backends.DjangoFilterBackend',),
}
