# -*- coding: utf-8 -*-
import pytz
from datetime import timedelta, datetime
import recurrence
from recurrence import (
    Recurrence,
    Rule,
)
from tests.utils import ChaosTestCase
from django.contrib.auth import get_user_model
from django.utils.timezone import now
from model_mommy import mommy
from chaos.models import (
    ProjectType,
    Project,
    TaskStatus,
    Task,
    Comment,
    TaskAttachment,
    Reminder,
)
User = get_user_model()


class ReminderTestCase(ChaosTestCase):

    def test_get_reminders_with_recurrence(self):

        user = mommy.make(User)
        proj = mommy.make(Project)
        delta = timedelta(days=1)
        start = now() + delta
        task = Task.objects.create(
            project=proj,
            name='foo',
            start_date=start,
            end_date=start + delta
        )

        recur = Recurrence(
            rrules=[
                Rule(recurrence.DAILY, interval=1)
            ]
        )

        reminder = Reminder(
            content_object=task,
            user=user,
            time_delta=delta,
            repeat=recur
        )
        reminder.save()
        dates = reminder.reminder_dates
        self.assertIsNotNone(dates)
        self.assertEqual(len(dates), 2)

    def test_get_reminders_without_start_date_returns_none(self):
        user = mommy.make(User)
        proj = mommy.make(Project)
        delta = timedelta(days=2)
        task = Task.objects.create(
            project=proj,
            name='foo',
        )
        reminder = Reminder(
            content_object=task,
            user=user,
            time_delta=delta
        )
        reminder.save()

        self.assertIsNotNone(reminder)
        self.assertIsNone(reminder.reminder_dates)

    def test_get_reminders_with_start_without_recurrence_returns_single_date(self):
        user = mommy.make(User)
        proj = mommy.make(Project)
        delta = timedelta(days=2)
        start_date = now() + delta
        reminder_date = start_date - delta
        task = Task.objects.create(
            project=proj,
            name='foo',
            start_date=start_date
        )
        reminder = Reminder(
            content_object=task,
            user=user,
            time_delta=delta
        )
        reminder.save()

        self.assertIsNotNone(reminder)
        self.assertEqual(reminder_date, reminder.reminder_dates[0])


class ProjectTestCase(ChaosTestCase):

    def test_clone_project_no_end_dates(self):
        project = mommy.make(Project)
        start_date1 = datetime(2000, 1, 1, 0, 0, 0, tzinfo=pytz.utc)
        start_date2 = datetime(2000, 1, 10, 0, 0, 0, tzinfo=pytz.utc)
        new_date = start_date2 + timedelta(days=5)

        t1 = Task(
            start_date=start_date1,
            project=project,
            name='foo'
        )
        t1.save()
        
        t2 = Task(
            start_date=start_date2,
            project=project,
            name='foo'
        )
        t2.save()
        
        clone = project.clone(
            new_name='bar',
            new_date=new_date
        )
        self.assertIsNotNone(clone)
        self.assertEqual(clone.name, 'bar')
        self.assertEqual(clone.tasks.all()[0].start_date, new_date)
        self.assertEqual(clone.tasks.all()[1].start_date, new_date  + timedelta(days=9))

    def test_clone_project_no_start_dates(self):
        project = mommy.make(Project)
        end_date1 = datetime(2000, 1, 1, 0, 0, 0, tzinfo=pytz.utc)
        end_date2 = datetime(2000, 1, 10, 0, 0, 0, tzinfo=pytz.utc)
        new_date = end_date2 + timedelta(days=5)

        t1 = Task(
            end_date=end_date1,
            project=project,
            name='foo'
        )
        t1.save()
        
        t1 = Task(
            end_date=end_date2,
            project=project,
            name='foo'
        )
        t1.save()

        clone = project.clone(
            new_name='bar',
            new_date=new_date
        )
        self.assertIsNotNone(clone)
        self.assertEqual(clone.name, 'bar')
        self.assertEqual(clone.tasks.all()[0].end_date, new_date)
        self.assertEqual(clone.tasks.all()[1].end_date, new_date  + timedelta(days=9))

    def test_clone_project_no_dates(self):
        proj = mommy.make(Project)
        t1 = Task(project=proj, name='foo1')
        t1.save()
        t2 = Task(project=proj, name='foo2')
        t2.save()
        new_proj = proj.clone(new_name='new')
        self.assertIsNotNone(new_proj)
        self.assertEqual(new_proj.name, 'new')
        self.assertEqual(new_proj.tasks.all().count(), 2)
        self.assertEqual(new_proj.root_tasks.all().count(), 2)

    def test_clone_project_with_dates(self):
        proj = mommy.make(Project)
        start = datetime(2000, 1, 1, 0, 0, 0, tzinfo=pytz.utc)
        end = start + timedelta(days=10)
        new_start = end + timedelta(days=10)
        t1 = Task(
            project=proj,
            name='foo1',
            start_date=start,
            end_date=end
        )
        t1.save()
        t2 = Task(
            project=proj,
            name='foo2',
            start_date=start,
            end_date=end
        )
        t2.save()
        t3 = Task(
            parent=t2,
            project=proj,
            name='foo2 filha',
            start_date=start,
            end_date=end
        )
        t3.save()
        new_proj = proj.clone(new_name='new', new_date=new_start)
        self.assertIsNotNone(new_proj)
        self.assertEqual(new_proj.name, 'new')
        self.assertEqual(new_proj.tasks.all().count(), 3)
        self.assertEqual(new_proj.root_tasks.all().count(), 2)
        self.assertEqual(new_proj.tasks.all()[0].start_date, new_start)
        self.assertEqual(new_proj.tasks.all()[0].end_date, new_start + timedelta(days=10))
        self.assertEqual(new_proj.tasks.all()[1].start_date, new_start)
        self.assertEqual(new_proj.tasks.all()[1].end_date, new_start + timedelta(days=10))
        self.assertEqual(new_proj.tasks.all()[2].start_date, new_start)
        self.assertEqual(new_proj.tasks.all()[2].end_date, new_start + timedelta(days=10))


class TaskTestCase(ChaosTestCase):

    def test_related_tasks_should_return_task_attachments(self):
        proj = mommy.make(Project)
        t1 = Task(project=proj, name='foo1')
        t1.save()
        t2 = Task(project=proj, name='foo2')
        t2.save()
        t1.attach(t2)
        self.assertEqual(t1.related_tasks.all().count(), 1)

    def test_attach_without_model_raises_error(self):

        proj = mommy.make(Project)
        t1 = Task(project=proj, name='foo1')
        t1.save()
        with self.assertRaises(ValueError):
            t1.attach(None)

    def test_deattach_without_model_raises_error(self):

        proj = mommy.make(Project)
        t1 = Task(project=proj, name='foo1')
        t1.save()
        with self.assertRaises(ValueError):
            t1.detach(None)

    def test_attach_task(self):

        proj = mommy.make(Project)
        t1 = Task(project=proj, name='foo1')
        t1.save()
        t2 = Task(project=proj, name='foo2')
        t2.save()
        attachment = t1.attach(t2)
        self.assertEqual(t1.attachments.all().count(), 1)
        self.assertEqual(t1.pk, attachment.task_id)
        self.assertEqual(t2.pk, attachment.content_object.pk)

    def test_attach_twice_should_create_a_single_attachment(self):
        proj = mommy.make(Project)
        t1 = Task(project=proj, name='foo1')
        t1.save()
        t2 = Task(project=proj, name='foo2')
        t2.save()

        att1 = t1.attach(t2)

        self.assertEqual(t1.attachments.all().count(), 1)
        self.assertEqual(t1.pk, att1.task_id)
        self.assertEqual(t2.pk, att1.content_object.pk)

        att2 = t2.attach(t2)

        self.assertEqual(t1.attachments.all().count(), 1)
        self.assertEqual(att2.pk, att1.pk)

    def test_deattach_task(self):

        proj = mommy.make(Project)
        t1 = Task(project=proj, name='foo1')
        t1.save()
        t2 = Task(project=proj, name='foo2')
        t2.save()

        t1.attach(t2)
        t1.detach(t2)

        self.assertEqual(t1.attachments.all().count(), 0)

    def test_calculate_node_score(self):

        proj = mommy.make(Project)
        t1 = Task(project=proj, name='foo')
        t1.save()
        score = t1.score
        self.assertIsNotNone(score)
        self.assertEqual(score, 0)

    def test_clone_without_project_and_node_raises(self):

        proj1 = mommy.make(Project)
        proj2 = mommy.make(Project)
        t1 = Task(project=proj1, name='foo')
        t1.save()
        t2 = Task(project=proj2, name='bar')
        t2.save()

        with self.assertRaises(ValueError):
            t1.clone()

    def test_clone_other_project(self):
        proj1 = mommy.make(Project)
        proj2 = mommy.make(Project)
        t1 = Task(
            project=proj1,
            name='foo',
            start_date=now(),
            end_date=now()
        )
        t1.save()
        t2 = Task(project=proj2, name='bar')
        t2.save()

        self.assertEqual(proj2.tasks.all().count(), 1)

        t1.clone(
            new_project=proj2
        )

        self.assertEqual(proj2.tasks.all().count(), 2)
        self.assertEqual(proj2.root_tasks.all().count(), 2)

        copied = Task.objects.get(project=proj2, name=t1.name)
        self.assertIsNotNone(copied)
        self.assertEqual(copied.name, t1.name)
        self.assertEqual(copied.responsible, t1.responsible)
        self.assertEqual(copied.start_date, t1.start_date)
        self.assertEqual(copied.end_date, t1.end_date)
        self.assertEqual(t2.get_children().count(), 0)

    def test_clone_new_node(self):
        proj1 = mommy.make(Project)
        proj2 = mommy.make(Project)
        t1 = Task(
            project=proj1,
            name='foo',
            start_date=now(),
            end_date=now()
        )
        t1.save()
        t2 = Task(project=proj2, name='bar')
        t2.save()
        t1.clone(
            new_project=proj2,
            new_node=t2
        )

        self.assertEqual(proj2.tasks.all().count(), 2)
        self.assertEqual(proj2.root_tasks.count(), 1)
        self.assertEqual(t2.get_children().count(), 1)

        copied = Task.objects.get(project=proj2, name=t1.name)
        self.assertEqual(t2.pk, copied.parent.pk)

    def test_clone_new_node_with_new_date(self):
        proj1 = mommy.make(Project)
        proj2 = mommy.make(Project)
        start = now()
        end = start + timedelta(days=1)
        t1 = Task(
            project=proj1,
            name='foo',
            start_date=start,
            end_date=end
        )
        t1.save()
        t2 = Task(project=proj2, name='bar')
        t2.save()
        t1.clone(
            new_project=proj2,
            new_node=t2,
            new_date=end + timedelta(days=1)
        )

        self.assertEqual(proj2.tasks.all().count(), 2)
        self.assertEqual(proj2.root_tasks.count(), 1)
        self.assertEqual(t2.get_children().count(), 1)

        copied = Task.objects.get(project=proj2, name=t1.name)
        self.assertEqual(copied.parent.pk, t2.pk)
        new_end = end + timedelta(days=1)
        delta = copied.end_date - copied.start_date
        self.assertEqual(delta.days, 1)
        self.assertEqual(copied.start_date.day, new_end.day)

    def test_clone_with_attachments(self):
        proj1 = mommy.make(Project)
        proj2 = mommy.make(Project)
        t1 = Task(
            project=proj1,
            name='foo',
            start_date=now(),
            end_date=now()
        )
        t1.save()
        a1 = TaskAttachment(
            task=t1,
            content_object=proj1
        )
        a1.save()
        t2 = Task(project=proj2, name='bar')
        t2.save()
        t1.clone(
            new_project=proj2,
            new_node=t2,
            keep_attachments=True
        )

        copied = Task.objects.get(project=proj2, name=t1.name)
        self.assertEqual(TaskAttachment.objects.all().count(), 2)
        self.assertEqual(copied.attachments.all().count(), 1)
        self.assertEqual(a1.content_object.pk, copied.attachments.all()[0].object_id)

    def test_clone_with_comments(self):
        proj1 = mommy.make(Project)
        proj2 = mommy.make(Project)
        t1 = Task(
            project=proj1,
            name='foo',
            start_date=now(),
            end_date=now()
        )
        t1.save()
        c1 = mommy.make(Comment, task=t1)
        t2 = Task(project=proj2, name='bar')
        t2.save()
        t1.clone(
            new_project=proj2,
            new_node=t2,
            keep_comments=True
        )

        copied = Task.objects.get(project=proj2, name=t1.name)
        self.assertEqual(Comment.objects.all().count(), 2)
        self.assertEqual(copied.comments.all().count(), 1)
        self.assertEqual(c1.text, copied.comments.all()[0].text)
