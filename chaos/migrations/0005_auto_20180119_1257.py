# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2018-01-19 18:57
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('chaos', '0004_auto_20180112_1140'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='comment',
            options={'ordering': ('id',), 'verbose_name': 'Comment', 'verbose_name_plural': 'Comments'},
        ),
    ]
