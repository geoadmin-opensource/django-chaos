=====
Usage
=====

To use Django Chaos in a project, add it to your `INSTALLED_APPS`:

.. code-block:: python

    INSTALLED_APPS = (
        ...
        'chaos.apps.ChaosConfig',
        ...
    )

Add Django Chaos's URL patterns:

.. code-block:: python

    from chaos import urls as chaos_urls


    urlpatterns = [
        ...
        url(r'^', include(chaos_urls)),
        ...
    ]
