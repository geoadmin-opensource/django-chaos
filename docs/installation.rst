============
Installation
============

At the command line::

    $ easy_install django-chaos

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv django-chaos
    $ pip install django-chaos
